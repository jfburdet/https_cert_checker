#! /usr/bin/env python3

import datetime
import sys
import socket

import idna
from OpenSSL import SSL


def get_certificate(hostname, port):
    hostname_idna = idna.encode(hostname)
    sock = socket.socket()

    sock.connect((hostname, port))
    ctx = SSL.Context(SSL.SSLv23_METHOD)  # most compatible
    ctx.check_hostname = False
    ctx.verify_mode = SSL.VERIFY_NONE

    sock_ssl = SSL.Connection(ctx, sock)
    sock_ssl.set_connect_state()
    sock_ssl.set_tlsext_host_name(hostname_idna)
    sock_ssl.do_handshake()
    cert = sock_ssl.get_peer_certificate()
    sock_ssl.close()
    sock.close()

    return cert


def days_till_expire_soon(cert):
    cert_crypto = cert.to_cryptography()
    diff = cert_crypto.not_valid_after - datetime.datetime.now()

    return diff.days


def get_ips_from_name(hostname):
    # Let's fetch ips from list return by getaddrinfo
    adrs = list(map(lambda list_item: list_item[4][0], socket.getaddrinfo(host, 443)))

    # Remove duplicates
    return list(dict.fromkeys(adrs))


if __name__ == '__main__':

    # Sample usage in a cron :
    # * 7 * * * /home/jfburdet/bin/check_ssl.py www.camptocamp.org 14

    host = sys.argv[1]
    days = int(sys.argv[2])

    ips = get_ips_from_name(host)

    for ip in ips:
        cert = get_certificate(ip, 443)

        host_ip_to_print = host + "(" + ip + ")"

        if cert.has_expired():
            print("Warning, SSL certificate for " + host_ip_to_print + " has expired !")

        if days_till_expire_soon(cert) < days:
            cert_crypto = cert.to_cryptography()
            print("Warning, SSL certificate for " + host_ip_to_print + " will expire in less than " + str(
                days) + " days (" + str(
                cert_crypto.not_valid_after) + ").")